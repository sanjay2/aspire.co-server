import os
from flask import Flask, request, jsonify
from flask import Response, send_from_directory, redirect
import requests
import sqlite3
from flask import g
import json
app = Flask(__name__, static_url_path='')

from flask.ext.sqlalchemy import SQLAlchemy

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
db = SQLAlchemy(app)

@app.route("/")
def root():
    return app.send_static_file('index.html')

@app.route("/add/<token>", methods=['POST'])
def add(token):
    request.accept_mimetypes['application/json']
    email = verifyToken(token)
    data = request.json
    try:
        note = Notes(data['note'], email, data['created'])
        db.session.add(note)
        db.session.commit()
    except Exception as e:
        print e
    return jsonify(message = 'Note Added')

@app.route("/get/<token>", methods=['GET'])
def get(token):
    request.accept_mimetypes['application/json']
    email = verifyToken(token)
    raw = getNotes(email)
    res = jsonify(notes = raw)
    return res

def getNotes(email):
    notes = Notes.query.filter_by(email=email).order_by(Notes.created.desc()).all()
    return serialize(notes)

def serialize(notes):
    res = []
    for note in notes:
        res.append({'note': note.note, 'created': note.created})
    return res

def verifyToken(token):
    payload = {'id_token': token}
    r = requests.get('https://www.googleapis.com/oauth2/v3/tokeninfo', params=payload)
    return r.json()['email']

class Notes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.String(255))
    email = db.Column(db.String(120))
    created = db.Column(db.String(13))

    def __init__(self, note, email, created):
        self.note = note
        self.email = email
        self.created = created

    def __repr__(self):
        return "<Notes(note='%s', created='%s')>" % (self.note, self.created)

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8080))
    app.run(host='0.0.0.0', port=port)
