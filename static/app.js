'use strict';

const serverUrl = '/';

var app = angular.module('notesApp', [
  'ngMaterial',
  'google-signin',
  'ngCookies'
]);

app.config(['GoogleSigninProvider', function(GoogleSigninProvider) {
   GoogleSigninProvider.init({
      client_id: '815050883599-ob0dh5rl44q4fitgo2diedkjfe1qkf9e',
   });
}]);

app.controller('loginCtrl', ['$scope', 'GoogleSignin','UserService', '$rootScope',
    function($scope, GoogleSignin, UserService, $rootScope){
  var vm = this;
  $scope.login = function () {
    GoogleSignin.signIn().then(function (user) {
      UserService.setUserInfo(angular.copy(user));
      UserService.checkUser();
    }, function (err) {
        alert("An Error Occured.")
    });
  };
}]);

app.controller('acctCtrl', ['UserService', '$rootScope', '$scope',  function(UserService, $rootScope, $scope){
  $rootScope.$on('userLoggedIn', function(){
    $scope.username = UserService.getUsername();
    $scope.imgUrl = UserService.getUserPic();
  });
  $scope.logout = function() {
    UserService.removeToken();
    $rootScope.loggedIn = false;
  };
}]);

app.controller('notesCtrl', ['UserService', 'NotesService', '$scope', '$rootScope', '$mdDialog', 'dateFilter',
    function(UserService, NotesService, $scope, $rootScope, $mdDialog, dateFilter){
    $rootScope.$on('userLoggedIn', function(){
      NotesService.getNotes().then(function(res){

        if(res.data.notes.length === 0){
          $scope.notes = [{'note': 'Create your First Note', 'created': Date.now()}];
        } else {
          $scope.notes = res.data.notes;
        }
      });
    });
    $scope.newNote = "";
    $scope.addNote = function() {
      if(!$scope.newNote){
        $scope.newNote = 'Empty';
      }
      var data = {
        note: $scope.newNote,
        created: Date.now(),
      };
      NotesService.addNote(data).then(function(res){
        if(res.data.message === 'Note Added'){
          NotesService.getNotes().then(function(res){
            $scope.notes = res.data.notes;
            $scope.newNote = "";
          });
        }
      });
    };
    $scope.showAlert = function(ev, note) {
      $mdDialog.show(
        $mdDialog.alert()
          .parent(angular.element(document.querySelector('#notesContainer')))
          .clickOutsideToClose(true)
          .title('Note')
          .textContent(note.note + ' - ' + dateFilter(note.created))
          .ok('Got it!')
          .targetEvent(ev)
      );
    };
    UserService.checkUser();
}]);

app.service('UserService', ['$cookies', '$rootScope', function($cookies, $rootScope){
  this.setUserToken = setUserToken;
  this.setUsername = setUsername;
  this.setUserPic = setUserPic;
  this.getUserToken = getUserToken;
  this.getUsername = getUsername;
  this.getUserPic = getUserPic;
  this.setUserInfo = setUserInfo;
  this.checkUser = checkUser;
  this.removeToken = removeToken;

  function removeToken() {
    $cookies.remove('token');
  }

  function checkUser() {
    if(getUserToken()){
      $rootScope.loggedIn = true;
      $rootScope.$broadcast('userLoggedIn');
    }
  }

  function setUserInfo(googleUser){
    setUserToken(googleUser.getAuthResponse().id_token);
    setUsername(googleUser.getBasicProfile().getName());
    setUserPic(googleUser.getBasicProfile().getImageUrl());
  }

  function setUserToken(token){
    $cookies.put('token', token, {expiry: new Date(Date.now() + 30*60000)})
  }

  function setUsername(username){
    $cookies.put('username', username, {expiry: new Date(Date.now() + 30*60000)});
  }

  function setUserPic(url) {
    $cookies.put('userpic', url, {expiry: new Date(Date.now() + 30*60000)})
  }

  function getUserToken(){
    return $cookies.get('token')
  }

  function getUsername(){
    return $cookies.get('username');
  }

  function getUserPic() {
    return $cookies.get('userpic');
  }

}]);

app.service('NotesService', ['UserService', '$http', function(UserService, $http){
  this.getNotes = getNotes;
  this.addNote = addNote;

  function getNotes() {
    return $http.get(serverUrl + 'get/' + UserService.getUserToken());
  }

  function addNote(data) {
    return $http.post(serverUrl + 'add/' + UserService.getUserToken(), data);
  }
}]);
